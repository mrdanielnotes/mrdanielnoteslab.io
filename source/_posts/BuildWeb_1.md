---
title: 架設網站三部曲(1/3)
tags: [npm, git, hexo]
categories: [免費架站]
date: 2020-07-27 12:00:00
---

使用免費的工具，架設屬於自己的部落格，讓世界看見你

<!-- more-->

# 前置作業

我們以windows做範例，以下是需要的三個安裝

1. npm (Node Package Manager)
2. git
3. hexo

---



稍微提到一下有關於"命令提示字元"又稱，小黑窗、終端機、cmd等暱稱，下方就以**小黑窗**作為代稱

開啟的方式有幾種方式，這裡簡單介紹一下我認為比較好用的兩種，如下

```python
1. windows + r
2. 輸入cmd，按下確定
```

<img src="../images/BuildWebsite/image-20200727103357458.png" alt="image-20200727103357458" style="zoom:50%;" />

````python
windows + q 或直接點選左下角的放大鏡，並輸入cmd
````

<img src="../images/BuildWebsite/image-20200727103554162.png" alt="image-20200727103554162" style="zoom:50%;" />



# Install

## [Node Package Manager(Nodejs)](https://nodejs.org/en/download/)

LTS = 推薦給大部分的使用者，也就是 **[穩定版]**

Current = **[最新版]**

個人是覺得，求穩就好，所以安裝LTS的部分

下面有三個系統選項，這裡直接點選**windows installer**，點下去就可以下載，下載完成，按照預設路徑安裝

<img src="../images/BuildWebsite/image-20200727104920920.png" alt="image-20200727104920920" style="zoom:50%;" />

打開小黑窗 (命令提示字元、終端機) 測試一下

````python
windows + r,  輸入cmd
````

查看是否有npm這個指令和版本

````PYTHON
npm -v
````

出現下方圖代表已成功安裝，版本不相同沒關係

![image-20200727105542811](../images/BuildWebsite/image-20200727105542811.png)

**如果沒有成功，可以把小黑窗重開或重新安裝，再操作一次** 

---



## [git](https://git-scm.com/downloads) 

Downloads的區塊，選擇你的作業系統安裝，下載完成後，依照預設路徑安裝即可

<img src="../images/BuildWebsite/image-20200727105854390.png" alt="image-20200727105854390" style="zoom:50%;" />

打開小黑窗 (命令提示字元、終端機) 測試一下

````python
windows + r,  輸入cmd
````

查看是否有git這個指令和版本

````PYTHON
git --version
````

出現下方圖代表已成功安裝，版本不相同沒關係

![image-20200727110218566](../images/BuildWebsite/image-20200727110218566-1596620091993.png)

**如果沒有成功，可以把小黑窗重開，再操作一次**

---



## Hexo

在小黑窗中輸入下方列，就會開始安裝 Hexo

````python
npm install -g hexo-cli
````

安裝完成畫面

![image-20200727110622453](../images/BuildWebsite/image-20200727110622453-1596620106469.png)

進行測試，直接輸入

````python
hexo version
````

可以直接看到版本，代表完成

![image-20200727110740649](../images/BuildWebsite/image-20200727110740649-1596620117967.png)

---



# Conclusion

安裝以下三個套件，並確認安裝完成，也可使用指令

1. [npm](https://nodejs.org/en/download/)
2. [git](https://git-scm.com/download)
3. hexo

打完收工，下一篇開始教學，架設專屬於你自己的網站

