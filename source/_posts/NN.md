---
title: Neural Network 類神經網路 - 到底在紅什麼
date: 2020-08-31 11:12:43
tags: [Machine Learning, Neural Network]
categories: [Artificial intelligence]
---

類神經網路要理解非常簡單，只要記住幾句話就能夠理解

<!-- more -->

# 前言

想像一下小朋友在學習的環境，給他一個任務(目標)，讓他去執行，過程中會有獎勵、懲罰的反饋機制

1. 給予的任務
2. 反饋/獎懲

# 舉例來說

教小朋友認識動物

<img src="../images/nn/image-20200831115545350.png" alt="image-20200831115545350" style="zoom: 33%;" />

這是我們的教學過程，圖中我們很清楚的知道，這樣的模式，就是 **Neural Network (類神經網路)** 學習的模式，以範例圖解釋

1. 給予的任務：辨識動物
2. 反饋/獎懲：糖果

經過多次的練習，小朋友就可以學習到，怎樣的形狀或圖示叫做貓咪，怎樣的圖示叫做狗

譬如說：貓咪的鬍鬚比較明顯、貓咪的耳朵是尖的、狗的鼻子比較長之類的

而這些也是 **Neural Network (類神經網路)** 稱之的 **Feature (特徵)** ，怎樣的 **Feature (特徵)** 或怎樣的 **形狀** 讓你認為他是貓或是狗

根據這些獲得的 **Feature (特徵)** 做辨識就是 **Neural Network (類神經網路)** 最重要的部分

因此我們可以知道完整的 **Neural Network (類神經網路)** 架構是

<img src="../images/nn/NN_Temp2_org.png" alt="NN_Temp2_org" style="zoom: 67%;" />

帶入剛剛的例子說明架構

<img src="../images/nn/NN_Temp2_Explain.png" alt="NN_Temp2_Explain" style="zoom:67%;" />

就能夠完整解釋 **Neural Network類神經網路** 的運作

以人類角度說明：拿到了 **一張** 貓/狗的圖片 > 思考一下這是貓還是狗 > 喔! 這是貓

# 類神經網路運作與說明

此小節主要在介紹架構的細節與運作，挺不住的朋友們，可以直接跳過這個環節，往下看到結論，你會獲得更好的文章體驗

## 架構介紹

<img src="../images/nn/NN_Temp2_org.png" alt="NN_Temp2_org" style="zoom: 67%;" />

Weight：權重，連接每一個神經元的線，都會有一個數值，該數值為 **Weight**

Feature：特徵，依形狀臉型，可以辨識貓與狗，而臉型就稱之為 **Feature**

Target：任務目標，訓練一個 **Neural Network 類神經網路** ，為了要分辨貓與狗， **分辨貓與狗的輸出稱為Target**

Neural：神經元， X<sub>0</sub> 至 X <sub>7</sub> 、W <sub>0</sub> 至 W<sub>9</sub>、Y <sub>0</sub> 、 Y<sub>1</sub> ，說白了就是一個數值，只是用神經元比較好對他的架構和傳導方式理解

Input Layer   ：圖示最右側 (輸入層)，也就是圖形的 **Feature**

Hidden Layer：圖示中間 (隱藏層)，以人類的角度來說， **"讓我想想"** ，完全呈現了隱藏層的運作

Output Layer：圖示最右側 (輸出層)，根據不同的 **Task** 所獲得的 **Target** 也會有所不同，可以有多個



## 實務上常見英文與參數介紹

Optimizer

- Adam (Adaptive Moment Estimation) 
- 隨機梯度下降 (Stochastic ggradient descents, SGD)

Loss Function：

- 平均方差 (Mean Square Error, MSE) 
- 平均絕對值差 (Mean Absoluate Error) 
- 交叉熵 (Cross Entropy) 

Activation Function：

- Sigmoid
- Tanh
- Relu
- Leaky Relu

Loss Value：誤差值，越低越好，與正確的目標差距多少的數值

Precious：精準度，越高越好，所謂的正確率

Learning Rete：學習率，更新Weight的比率

- (無限制，多半為 0~1)，太大會造成難以收斂，Loss Value一直居高不下，太小則造成訓練過慢，學習太慢、效果不彰

Decay：根據Epoch和每一次訓練所遞減的百分比數值為

-  (預設為1，大多設定為 0.8~0.99)，預防過度擬合的方法之一

Epoch：疊代、迴圈次數、訓練次數，控制資料需要訓練幾次

- 亦可以根據Loss Value或Precious做調整和監控，達到一定的水準後，若還持續訓練，將會產生 Overfitting (過度擬和) 

Batch Size：每次訓練，多少筆數據為一綑做訓練

- 會影響到 Neural Network架構中的神經元個數和訓練結果，也是一個造成 Overfitting (過度擬和) 的原因之一

# 結論

識別一個物體、照片、分類正確等，皆是如此，經過訓練、學習、判定，獲得最後的結果

由於目前機器學習的方式，只是模仿人類學習的模式，仍然有許多不完善的地方，也有許多高手正在著手解決這些問題，演算法的改進、架構的優化、參數優化、自動調整參數、自動訓練等

常見的有：Tune Parameter (調整參數) , Overfitting (過度擬和) , Vanishing Gradient (梯度消失)  , Vanishing Exploding (梯度爆炸) 等

若能夠解決這些問題或調整參數的方式能夠更強大，想必未來世界已經近在眼前了

人工智慧再也不會是侷限一個領域的專家

